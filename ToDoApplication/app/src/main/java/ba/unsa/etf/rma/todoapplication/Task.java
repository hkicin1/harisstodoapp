package ba.unsa.etf.rma.todoapplication;

import java.time.LocalDate;

public class Task {
    private String id;
    private LocalDate dateCreated;
    private LocalDate dateFinished;
    private String title;
    private Status status;
    private String taskDescription;

    public Task() {
    }

    public Task(String id, LocalDate dateCreated, LocalDate dateFinished, Status status, String title, String taskDescription) {
        this.id = id;
        this.dateCreated = dateCreated;
        this.dateFinished = dateFinished;
        this.status = status;
        this.title = title;
        this.taskDescription = taskDescription;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDate dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDate getDateFinished() {
        return dateFinished;
    }

    public void setDateFinished(LocalDate dateFinished) {
        this.dateFinished = dateFinished;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    @Override
    public String toString() {
        return "Title='" + title + '\'' +
                ", status=" + status;
    }
}
