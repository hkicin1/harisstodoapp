package ba.unsa.etf.rma.todoapplication;

public enum Status {
    DONE(1), INPROGRESS(2);

    private int value;

    Status(int i) {
        this.value = i;
    }

    public static Status getStatusOf (int id) {
        if (id == 1) return DONE;
        return INPROGRESS;
    }

    public static Status getStatusIdOf (String name) {
        if (name.equals("DONE")) return DONE;
        return INPROGRESS;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
