package ba.unsa.etf.rma.todoapplication;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.util.ArrayList;

public class TaskInteractor extends AsyncTask<String, Integer, Void> {
    private static String Url = "https://crudcrud.com/api/af00c49e16ad46eb8dbd3e9abbc49c34/tasks";
    private OnAction caller;
    private OnGetAll getAllCaller;
    private ArrayList<Task> tasks = new ArrayList<>();
    private String type = "get";

    public TaskInteractor(OnAction caller, OnGetAll getAll) {
        this.caller = caller;
        this.getAllCaller = getAll;
    }

    public interface OnAction {
        void OnDelete();
        void OnEdit();
    }

    public interface OnGetAll {
        void OnAdd(ArrayList<Task> tasks);
        void GetTasks(ArrayList<Task> tasks);
    }

    @Override
    protected Void doInBackground(String... tasks) {
        try {
            String t = URLEncoder.encode(tasks[0], "utf-8");
            type = t;
            if (t.equals("get")) {
                GetAllTasks();
                return null;
            }
            String id = URLEncoder.encode(tasks[1], "utf-8");
            if (t.equals("delete")) {
                DeleteTask(id);
                return null;
            }
            String title = URLEncoder.encode(tasks[2], "utf-8");
            String desc = URLEncoder.encode(tasks[3], "utf-8");
            if (t.equals("add")) {
                AddTask(title, desc);
            }
            else if (t.equals("edit")) EditTask(id, title, desc);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void GetAllTasks() {
        try {
            URL url = new URL(Url);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = new BufferedInputStream(connection.getInputStream());
            String result = convertStreamToString(inputStream);
            JSONArray array = new JSONArray(result);
            for (int j = 0; j < array.length(); j++) {
                JSONObject obj = array.getJSONObject(j);
                String id = obj.getString("_id");
                String title = obj.getString("title");
                String body = obj.getString("body");
                Task task = new Task(id, LocalDate.now(), LocalDate.now(), ba.unsa.etf.rma.todoapplication.Status.DONE, title, body);
                tasks.add(task);
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    private void DeleteTask(String id) {
        HttpURLConnection con = null;
        java.net.URL url = null;
        try {
            url = new URL(Url + "/" + id);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");
            con.setRequestProperty("Content-Type", "application/json");
            con.setDoOutput(true);
            DataOutputStream out = new DataOutputStream(con.getOutputStream());
            out.flush();
            out.close();

            BufferedReader entry = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String json = "", line = "";
            while ((line = entry.readLine()) != null) {
                json = json + line;
            }
            entry.close();
            con.disconnect();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void EditTask(String id, String title, String desc) {
        HttpURLConnection con = null;
        java.net.URL url = null;
        try {
            url = new URL(Url + "/" + id);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            JSONObject object = new JSONObject();
            object.put("title", title);
            object.put("body", desc);
            byte[] data = object.toString().getBytes();
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("PUT");
            con.setRequestProperty("Content-Type", "application/json");
            con.setDoOutput(true);
            DataOutputStream out = new DataOutputStream(con.getOutputStream());
            out.write(data, 0, data.length);
            out.flush();
            out.close();

            BufferedReader entry = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String json = "", line = "";
            while ((line = entry.readLine()) != null) {
                json = json + line;
            }
            entry.close();
            con.disconnect();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    private void AddTask(String title, String desc) {
        HttpURLConnection con = null;
        java.net.URL url = null;
        try {
            url = new URL(Url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            JSONObject object = new JSONObject();
            object.put("title", title);
            object.put("body", desc);
            byte[] data = object.toString().getBytes();
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            con.setDoOutput(true);
            DataOutputStream out = new DataOutputStream(con.getOutputStream());
            out.write(data, 0, data.length);
            out.flush();
            out.close();

            BufferedReader entry = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String json = "", line = "";
            while ((line = entry.readLine()) != null) {
                json = json + line;
            }
            entry.close();
            con.disconnect();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        GetAllTasks();
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        switch (type) {
            case "get":
                getAllCaller.GetTasks(tasks);
                break;
            case "add":
                getAllCaller.OnAdd(tasks);
                break;
            case "delete":
                caller.OnDelete();
                break;
            default:
                caller.OnEdit();
                break;
        }
    }
}
