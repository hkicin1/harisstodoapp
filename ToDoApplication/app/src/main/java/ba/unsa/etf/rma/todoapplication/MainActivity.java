package ba.unsa.etf.rma.todoapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements TaskInteractor.OnGetAll {
    private ConnectivityBroadcastReceiver receiver =new ConnectivityBroadcastReceiver();
    private IntentFilter filter =new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");

    public ListView listOfTasks;
    public Button addTask;

    private ArrayAdapter<Task> listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listOfTasks = findViewById(R.id.listOfTasks);
        addTask = findViewById(R.id.add);

        listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new ArrayList<Task>());
        listOfTasks.setAdapter(listAdapter);
        listOfTasks.setOnItemClickListener(listItemClickListener);
        listOfTasks.setNestedScrollingEnabled(true);

        new TaskInteractor(null, (TaskInteractor.OnGetAll)this).execute("get");

        addTask.setOnClickListener(addTaskListener);
    }

    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Task task = listAdapter.getItem(position);
            Intent intent = new Intent(MainActivity.this, TaskDetailActivity.class);
            assert task != null;
            //String dateCreated = LocalDate.parse(task.getDateCreated().toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd")).toString();
            //String dateFinished = null;
            //if (task.getDateFinished() != null)
              //  dateFinished = LocalDate.parse(task.getDateFinished().toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd")).toString();

            intent.putExtra("id", task.getId());
            //intent.putExtra("dateCreated", dateCreated);
            //intent.putExtra("dateFinished", dateFinished);
            intent.putExtra("title", task.getTitle());
            //intent.putExtra("status", task.getStatus().toString());
            intent.putExtra("taskDescription", task.getTaskDescription());

            MainActivity.this.startActivity(intent);
        }
    };

    private View.OnClickListener addTaskListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AddNewTask();
        }
    };

    private void AddNewTask() {
        new TaskInteractor(null, (TaskInteractor.OnGetAll)this).execute("add", "1", "title", "desc");
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(receiver);
        super.onPause();
    }


    @Override
    public void OnAdd(ArrayList<Task> tasks) {
        listAdapter.clear();
        listAdapter.addAll(tasks);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void GetTasks(ArrayList<Task> tasks) {
        listAdapter.clear();
        listAdapter.addAll(tasks);
        listAdapter.notifyDataSetChanged();
    }
}