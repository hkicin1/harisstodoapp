package ba.unsa.etf.rma.todoapplication;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.time.format.DateTimeFormatter;

public class TaskDetailActivity extends AppCompatActivity implements TaskInteractor.OnAction{
    public EditText title;
    public EditText desc;
    public Button delete;
    public Button edit;
    private Task task = new Task();

    DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);

        title = findViewById(R.id.title);

        desc = findViewById(R.id.description);
        delete = findViewById(R.id.delete);
        edit = findViewById(R.id.edit);

        task.setId(getIntent().getStringExtra("id"));
        task.setTitle(getIntent().getStringExtra("title"));
        task.setTaskDescription(getIntent().getStringExtra("taskDescription"));

        title.setText(task.getTitle());
        desc.setText(task.getTaskDescription());
        delete.setOnClickListener(deleteTaskListener);
        edit.setOnClickListener(editTaskListener);
    }

    private View.OnClickListener deleteTaskListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DeleteTask(task.getId());
        }
    };

    private void DeleteTask(String id) {
        new TaskInteractor((TaskInteractor.OnAction)this, null).execute("delete", id);
    }

    private View.OnClickListener editTaskListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ConnectivityManager cm = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            boolean hasInternet = true;
            assert cm != null;
            if (cm.getActiveNetworkInfo() == null)
                hasInternet = false;

            final String titleToEdit = String.valueOf(title.getText());
            //LocalDate createDate = LocalDate.parse(crDate.getText(), df);
            //LocalDate finishedDate = LocalDate.parse(finDate.getText(), df);
            //String stat = String.valueOf(status.getText());
            final String description = String.valueOf(desc.getText());

            Task newTask = new Task();
            newTask.setTitle(titleToEdit);
            newTask.setTaskDescription(description);
            //newTask.setStatus(Status.valueOf(stat));
            //newTask.setDateCreated(createDate);
            //newTask.setDateFinished(finishedDate);
            if (hasInternet)
                EditTaskOnline(titleToEdit, description);
        }
    };

    private void EditTaskOnline(String titleToEdit, String description) {
        new TaskInteractor((TaskInteractor.OnAction)this, null).execute("edit", task.getId(), titleToEdit, description);
    }

    @Override
    public void OnDelete() {
        Intent returnToMainActivity = new Intent(getBaseContext(), MainActivity.class);
        startActivity(returnToMainActivity);
    }

    @Override
    public void OnEdit() {
        Intent returnToMainActivity = new Intent(getBaseContext(), MainActivity.class);
        Toast.makeText(getBaseContext(), "Task edited!", Toast.LENGTH_SHORT).show();

        startActivity(returnToMainActivity);
    }
}
